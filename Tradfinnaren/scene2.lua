---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

---------------------------------------------------------------------------------

local nextSceneButton
local startSceneButton

--ToDo: Fetch all info in the list via json and the magic powers of internet
local theList = {
	{ sv = "Gran", la = "Picea abies" },
	{ sv = "Tall", la = "Pinus silvestris" },
	{ sv = "Ask", la = "Fraxinus excelsior" },
	{ sv = "Bok", la = "Fagus sylvatica" },
	{ sv = "Björk",la = "Betula pendula" },
	{ sv = "Hästkastanj", la = "Aesculus hippocastanum" },
	{ sv = "Lind", la = "Tilia cordata" },
	{ sv = "Lönn", la = "Acer platanoides" },
	}

function scene:create( event )
    -- Called when the scene's view does not exist
    -- 
    local sceneGroup = self.view
    local tableView

	leftRect = display.newGroup()
	leftRect = self:getObjectByName( "UpperLeftRect" )
	rightRect = display.newGroup()
	rightRect = self:getObjectByName( "UpperRightRect" )
	

    
end



function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen
 
		local widget = require( "widget" )
		

		local function onRowRender( event )
		-- This function is called below, when the tableView is created
		-- Populate the table with info from theList
	
			-- Get reference to the row group
			local row = event.row

			-- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
			local rowHeight = row.contentHeight
			local rowWidth = row.contentWidth

			-- Display images in row
			local rowImage1 = display.newImageRect("images/0" .. row.index .. "_whole.png", 80, 80)
			row:insert(rowImage1)
			rowImage1.x = 70
			rowImage1.y = rowHeight / 2	
			local rowImage2 = display.newImageRect("images/0" .. row.index .. "_detail.png", 80, 80)
			row:insert(rowImage2)
			rowImage2.x = rowImage1.x + 80	 + 5
			rowImage2.y = rowImage1.y	

			-- Display text in row
			local rowTitle = display.newText( 
				{
				parent = row, 
				text = theList[row.index].sv,
				x = 0,
				y = 0,
				width = rowWidth,
				height = rowHeight, 
				font = nil,
				fontSize = 10, 
				align = "right"
				}
			) --rowTitle
			rowTitle:setFillColor( 0 )
			-- Align the label and vertically centered
			rowTitle.anchorX = 0
			rowTitle.y = rowHeight 

			
		end	 --onRowRender




		local function displayImages( treeNumber )
			--Display the images and info about tree treeNumber
			display.remove(upperLeftImage)
			display.remove(upperRightImage)
			upperLeftImage = display.newImageRect( "images/0" .. treeNumber .. "_detail.png", 384, 384  )
			
			function upperLeftImage:touch (event) 
  				print("upperLeft touch")
  				local function onComplete( event )
					local photo = event.target
					print( "photo w,h = " .. photo.width .. "," .. photo.height )
					photo.width = 384
					photo.height = 384
					photo.x = 448
					photo.y = 193
				end --onComplete

				if media.hasSource( media.Camera ) then
					media.capturePhoto( { listener=onComplete } )
				else
					native.showAlert( "Corona", "This device does not have a camera.", { "OK" } )
				end
  			end --upperLeftImage:touch
  			upperLeftImage.x = 448
  			upperLeftImage.y = 193
  			
  			upperLeftImage:addEventListener( "touch", upperLeftImage )
			upperRightImage = display.newImageRect( "images/0" .. treeNumber .. "_whole.png", 384, 384  )
			function upperRightImage:touch (event) 
  				print("upperRight touch")
  				local function onComplete( event )
					local photo = event.target
					print( "photo w,h = " .. photo.width .. "," .. photo.height )
					photo.width = 384
					photo.height = 384
					photo.x = 830
					photo.y = 193
				end --onComplete

				if media.hasSource( media.Camera ) then
					media.capturePhoto( { listener=onComplete } )
				else
					native.showAlert( "Corona", "This device does not have a camera.", { "OK" } )
				end
  			end --upperRightImage:touch
  			upperRightImage.x = 830
  			upperRightImage.y = 193
  			upperRightImage:addEventListener( "touch", upperRightImage )

		end --displayImages


		
		local function showTree( selectedRow )
	
			--Print to Terminal
			print( "Selected row: ", selectedRow )
			
			--Get references to objects on screen
        	TextHeader = self:getObjectByName( "txtHeader" )

        	TextHeader.text = "Welcome"
        	
        	--Do different things depending on what button is pressed. 
			if selectedRow == 1 then
				--Display the images and info about tree 1
				displayImages(1)
				TextHeader.text = "You pressed " .. theList[selectedRow].sv .. ", also called " .. theList[selectedRow].la .. "."
			elseif selectedRow == 2 then
				--Display the images and info about tree 2
				displayImages(2)
				TextHeader.text = "Now you pressed " .. theList[selectedRow].sv .. ", right? More latin? " .. theList[selectedRow].la .. "."
			elseif selectedRow == 3 then
				--Display the images and info about tree 3
				displayImages(3)
				TextHeader.text = "This time, " .. theList[selectedRow].sv .. ". " .. theList[selectedRow].la .. "."
			elseif selectedRow == 4 then
				--Display the images and info about tree 4
				displayImages(4)
				TextHeader.text = "Guess what – it's " .. theList[selectedRow].sv .. ". Some say: " .. theList[selectedRow].la .. "."
			elseif selectedRow == 5 then
				--Display the images and info about tree 5
				displayImages(5)
				TextHeader.text = "Wee! " .. theList[selectedRow].sv .. "! Hooray! " .. theList[selectedRow].la .. "."
			elseif selectedRow == 6 then
				--Display the images and info about tree 6
				displayImages(6)
				TextHeader.text = "We are working through theList: " .. theList[selectedRow].sv .. ". AKA " .. theList[selectedRow].la .. "."
			elseif selectedRow == 7 then
				--Display the images and info about tree 7
				displayImages(7)
				TextHeader.text = "Here's... " .. theList[selectedRow].sv .. "! And why not " .. theList[selectedRow].la .. "."
			elseif selectedRow == 8 then
				--Display the images and info about tree 8
				displayImages(8)
				TextHeader.text = "OK, this is what we do for " .. theList[selectedRow].sv .. ": " .. theList[selectedRow].la .. "."
			end --if
		
		end --showTree
		

		local function onRowTouch( event )
		-- This function is called below, when the tableView is created
		-- This is what happens when a row is pressed

		    local phase = event.phase

		    if "press" == phase then
		    	-- Something is pressed, a Row
        		selectedRow = event.target.index
        		showTree(selectedRow)
        		
			end
			
		end	--onRowTouch		


		
		-- Create the left hand Table widget 
		tableView = widget.newTableView
		{
			--other optional parameters: id, x, y, friction, maxVelocity, isLocked, isBounceEnabled, rowTouchDelay
			left = -20,
		    top = 0,
		    height = 768,
		    width = 256+20,
		    onRowRender = onRowRender,
		    onRowTouch = onRowTouch,
		    listener = scrollListener,
		    rowTouchDelay = 0
		}

		-- Insert 8 rows in the table
		for i = 1, 8 do
		    -- Insert a row into the tableView
		    local rowHeight = tableView.height / 8 -- equals around 100
		    local rowColor = { default={ 0.8, 0.8, 0.1*i, 0.8 } }
        	local lineColor = { 1, 1, 0 }
		    local isCategory = false		    
			tableView:insertRow(
			{
				--Other optional parameters, not used: id, params
				rowHeight = rowHeight,
				rowColor = rowColor,
				lineColor = lineColor,
				isCategory = isCategory,
		    	}
			)
		end --for
        

        
        
        
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc

        nextSceneButton = self:getObjectByName( "gotoMap" )
        if nextSceneButton then
        	-- touch listener for the button
        	function nextSceneButton:touch ( event )
        		local phase = event.phase
        		if "ended" == phase then
        			composer.gotoScene( "scene3", { effect = "fade", time = 300 } )
        		end
        	end
        	-- add the touch event listener to the button
        	nextSceneButton:addEventListener( "touch", nextSceneButton )
        end --if nextSceneButton
 
         startSceneButton = self:getObjectByName( "gotoStart" )
        if startSceneButton then
        	-- touch listener for the button
        	function startSceneButton:touch ( event )
        		local phase = event.phase
        		if "ended" == phase then
        			composer.gotoScene( "scene1", { effect = "fade", time = 300 } )
        		end
        	end
        	-- add the touch event listener to the button
        	startSceneButton:addEventListener( "touch", startSceneButton )
        end --if nextSceneButton
       
        
        
    end --if/elseif phase
    
    
end --function scene:show

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)

    	if tableView then
	        tableView:removeSelf()
	        tableView = nil
	    end




    elseif phase == "did" then
        -- Called when the scene is now off screen
		if nextSceneButton then
			nextSceneButton:removeEventListener( "touch", nextSceneButton )
		end
		if startSceneButton then
			startSceneButton:removeEventListener( "touch", startSceneButton )
		end
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
    

    
    
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
