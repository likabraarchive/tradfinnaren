---------------------------------------------------------------------------------
--
-- scene.lua
--
---------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

---------------------------------------------------------------------------------

local nextSceneButton
local myMap
local locationText




function scene:create( event )
    local sceneGroup = self.view

    -- Called when the scene's view does not exist
    -- 
    -- INSERT code here to initialize the scene
    -- e.g. add display objects to 'sceneGroup', add touch listeners, etc
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen
        
		 -- Create a native map view
		myMap = native.newMapView( 0, 0, 768, 768 )
		myMap.x = display.contentCenterX + 128
		myMap.y = display.contentCenterY
		
		-- Display map as vector drawings of streets (other options are "satellite" and "hybrid")
		myMap.mapType = "satellite"
		
		local attempts = 0
		
		locationText = display.newText( "Location: ", 0, 400, native.systemFont, 16 )
		locationText.anchorY = 0
		locationText.x = display.contentCenterX
		
		local function locationHandler( event )
		
		    local currentLocation = myMap:getUserLocation()
		
		    if ( currentLocation.errorCode or ( currentLocation.latitude == 0 and currentLocation.longitude == 0 ) ) then
		        locationText.text = currentLocation.errorMessage
		
		        attempts = attempts + 1
		
		        if ( attempts > 10 ) then
		            native.showAlert( "No GPS Signal", "Can't sync with GPS.", { "Okay" } )
		        else
		            timer.performWithDelay( 1000, locationHandler )
		        end
		    else
		        locationText.text = "Current location: " .. currentLocation.latitude .. "," .. currentLocation.longitude
		        myMap:setCenter( currentLocation.latitude, currentLocation.longitude )
		        --myMap:addMarker( currentLocation.latitude, currentLocation.longitude )
		    end
		end
		
		locationHandler()    
		
		-- End create map   
        
        
        
        
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
        
        
        nextSceneButton = self:getObjectByName( "GoToScene1Btn" )
        if nextSceneButton then
        	-- touch listener for the button
        	function nextSceneButton:touch ( event )
        		local phase = event.phase
        		if "ended" == phase then
        			composer.gotoScene( "scene2", { effect = "fade", time = 300 } )
        		end
        	end
        	-- add the touch event listener to the button
        	nextSceneButton:addEventListener( "touch", nextSceneButton )
        end
        
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
		if nextSceneButton then
			nextSceneButton:removeEventListener( "touch", nextSceneButton )
		end

	    if myMap then
	        myMap:removeSelf()
	        myMap = nil
	    end
	    if locationText then
	        locationText:removeSelf()
	        locationText = nil
	    end
		
		
		
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
    
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
